window.addEventListener('DOMContentLoaded', async () =>{

    const url = "http://localhost:8000/api/conferences/";

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        // console.log(data)
        // console.log(data.conferences);

        let selectTag = document.getElementById("conference")
        for(let conference of data.conferences){
            let option = document.createElement("option");
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    }

    const formTag = document.getElementById("create-conference-form");

    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const select = document.getElementById('conference');
        // console.log(select);
        // console.log(select.options);
        // console.log(select.selectedIndex);
        // console.log(select.options[select.selectedIndex].value);

        //const conferenceUrl = `http://localhost:8000/api/conferences/${select.options[select.selectedIndex].value}/presentations/`; This one uses conference.id 
        const conferenceUrl = `http://localhost:8000${select.options[select.selectedIndex].value}presentations/`;
        //console.log(conferenceUrl);


        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }

    });


});

function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
        <div class="col h-auto">
        <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${starts} - ${ends}</div>
        </div>
        </div>
        
    `
}

function createPlaceholder(){
    return `
    <div class="imagePlaceholder">
    <div class="col h-auto placeholder-glow">
        <div class="card mb-3 shadow placeholder-glow">
            <img src="./images/nature.jpeg" alt="" class="card-img-top placeholder-glow">
            <div class="card-body plaeholder-glow">
                <h5 class="card-title placeholder-glow">
                <span class="placeholder col-6"></span>
                </h5>
                <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
                </p>
            </div>
            <div clas="card-footer placeholder-glow">
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
            </div>
        </div>
    </div>
    </div>
    `

}

var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
function alert(message, type) {
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    alertPlaceholder.append(wrapper)
}


window.addEventListener('DOMContentLoaded', async () => {

const url = "http://localhost:8000/api/conferences/";

    // for(let i in data.conferences){
    //     let placeholderHtml = createPlaceholder();
    //     let column = document.querySelector(".row");
    //     column.innerHTML += placeholderHtml; 
    // }
    

    try {
        const response = await fetch(url);

        if (!response.ok) {
        // Figure out what to do when the response is bad
        alert('Oh no, you triggered this alert message!', 'danger')

        } else {
            const data = await response.json();
        
        // const conference = data.conferences[0];
        // //console.log(conference);
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name; 

        // const detailURL = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch(detailURL);
        // if (detailResponse.ok){
        //     const details = await detailResponse.json();
        //     //console.log(details);
        //     const detail = details.conference;
        //     const descriptionTag = document.querySelector('.card-text');
        //     descriptionTag.innerHTML = detail.description;

        //     const imageTag = document.querySelector('.card-img-top');
        //     imageTag.src = details.conference.location.picture_url;

        // }
            let placeholderHtml = createPlaceholder();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString(); 
                    const html = createCard(name, description, pictureUrl,location, starts, ends);
                    const column = document.querySelector('.row');
                    // column.innerHTML += placeholderHtml; 
                    column.innderHTML = "";
                    column.innerHTML += html;
                    
                    //console.log(html);
                    
                }
            }
        }

    } catch (e) {
        // Figure out what to do if an error is raised
        console.error('Error!', e);
        alert('Error! Invalid data!', 'danger')

    }

});